#pragma once

#include "bgsub/BGSubtractor.h"
#include "bgsub/Log.h"

#include <iostream>
#include <filesystem>
#include <string.h>
struct Config {
    std::filesystem::path bg_dir, fg_dir, out_dir;
    
    Verbosity verbosity;

    bool save_masks;
    std::filesystem::path masks_dir;

    int threads;
};

const char* help = R"(
This is the help for the background subtraction program.

You can use this program in different ways.

The easy option is to just provide 3 parameters in form of paths.
    - The first path points to the directory containing background images.
    - The second path points to the directory containing the foreground or normal images.
    - The last path points to an (prefferably) empty output directory.

If you want to specify more options you can use the following (* means mandatory):
    - '-bg' | '--background' <path/to/background_images>  *
    - '-fg' | '--foreground' <path/to/foreground_images>  *
    - '-o' | '--output' <path/to/output_directory>        *
    - '-v' | '--verbose' <1 (some) - 3 (all)> (default = 1, if used without level specification, than 2 is assumed)
    - '-sm' | '--save-masks' (default = no)
    - '-mp' | '--masks-path' <path/to/masks_directory (default = output directory)
    - '-t' | '--threads' <min 1 to max 32> (default = 8)

Examples:
    Easy option:
        ./bgsub ./images/bg ./images/fg ./results
    
    More options:
        ./bgsub -fg ./images/fg -bg ./images/bg -v -sm -mp ./results/masks -o ./results
        ./bgsub -fg ./images/fg -bg ./images/bg -v 3
)";

bool read_config(int argc, char* argv[], Config* config);
