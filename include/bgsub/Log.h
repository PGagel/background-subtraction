#pragma once

#include <cstdlib>
#include <ctime>
#include <string>

enum class Verbosity {
    NORMAL = 0,
    VERBOSE = 1,
    DEBUG = 2
};

class Log {
public:
    static Verbosity verbosity_level;

    static void log(std::string message);
    static void log_v(std::string message);
    static void log_d(std::string message);

private:
    static constexpr char prompt[3] = ">>";
    static constexpr char verbosity_strings[3][8] = {"NORMAL ", "VERBOSE", "DEBUG  "};

private:
    static void _log (Verbosity level, std::string message);
};