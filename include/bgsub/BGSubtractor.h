#pragma once

#include "bgsub/Log.h"

#include <vector>
#include <string>
#include <filesystem>
#include <map>

#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/core.hpp>

namespace{ namespace fs = std::filesystem; }
class BGSubtractor {
typedef std::vector<fs::path> Paths;
typedef std::vector<cv::Mat> Images;
public:
    BGSubtractor(fs::path bg_img_dir, fs::path normal_img_dir, fs::path out_dir, int threads = 8, bool save_mask = false, fs::path mask_dir = "");

    // start execution of loading, subtracting and saving images 
    int run();

private:
    fs::path m_bg_img_dir;      // Directory where all background images are stored.
    fs::path m_fg_img_dir;      // Directory where all foreground images are stored. 
    fs::path m_out_dir;         // Directory where results are going to be saved.

    bool m_save_masks;          // Toggle saving of Masks
    fs::path m_masks_dir;       // Directory where masks are going to be saved if m_save_masks == true
    Images m_masks;             // List of masks to save if necessary

    int m_threads;
private:
    // list all image paths in directory
    Paths get_images_in_path(fs::path path);
    // matches images according to their name
    std::map<fs::path, fs::path> match_images(const Paths& bg_image_paths, const Paths& normal_image_paths);
    // loads images into memory
    std::vector<std::pair<cv::Mat, cv::Mat>> load_images(const std::map<fs::path, fs::path>& matched_image_paths);
    // subtract images using algorithm provided by OpenCV 
    Images subtract_images(std::vector<std::pair<cv::Mat, cv::Mat>>& matched_images);
    // subtract images using naive algorithm where pixel values are compared
    Images subtract_images_naive(const std::vector<std::pair<cv::Mat, cv::Mat>>& matched_images);
    // save images to output directory
    int save_images(const std::vector<cv::Mat>& images);
};