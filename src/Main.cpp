#include "Main.h"

int main (int argc, char* argv[]) {
    Config config;
    config.save_masks = false;
    config.verbosity = Verbosity::NORMAL;
    config.masks_dir = "./";
    config.threads = 8;

    bool success = read_config(argc, argv, &config);

    if(success) {
        
        if(config.save_masks && !fs::exists(config.masks_dir)) {
            config.masks_dir = config.out_dir;
        }

        Log::verbosity_level = config.verbosity;
        BGSubtractor sub(config.bg_dir, config.fg_dir, config.out_dir, config.threads, config.save_masks, config.masks_dir);
        int result = sub.run();

        std::stringstream ss;
        ss << "Images subtracted: " << result;
        Log::log(ss.str());
        return 0;
    }
    return 1;
}

bool read_config(int argc, char* argv[], Config* config) {

    if(argc == 0) {
        std::cout << "Too few parameters. Please use -h | --help for more information." << std::endl;
        return false;
    }

    // Simple mode
    if(argc == 4) {
        bool valid = std::filesystem::exists(argv[1]) || std::filesystem::exists(argv[2]) || std::filesystem::exists(argv[3]);
        if(valid) {
            config->bg_dir = argv[1];
            config->fg_dir = argv[2];
            config->out_dir = argv[3];
            config->verbosity = Verbosity::NORMAL;
            config->save_masks = false;
            config->masks_dir = "";
            return true;
        }
    }

    // Normal mode
    for(int i = 1; i < argc; i++) {
        char* arg = argv[i];
        if(strcmp(arg, "-bg") == 0 || strcmp(arg, "--background") == 0) {
            if(i+1 < argc && std::filesystem::exists(argv[i+1])) {
                config->bg_dir = argv[i+1];
                i++;
            } else {
                std::cout << "No background directory provided after background directory specifier." << std::endl;
                return false;
            }

        } else if(strcmp(arg, "-fg") == 0 || strcmp(arg, "--foreground") == 0) {
            if(i+1 < argc && std::filesystem::exists(argv[i+1])) {
                config->fg_dir = argv[i+1];
                i++;
            } else {
                std::cout << "No foreground directory provided after foreground directory specifier." << std::endl;
                return false;
            }

        } else if(strcmp(arg, "-o") == 0 || strcmp(arg, "--output") == 0) {
            if(i+1 < argc && std::filesystem::exists(argv[i+1])) {
                config->out_dir = argv[i+1];
                i++;
            } else {
                std::cout << "No output directory provided after output directory specifier." << std::endl;
                return false;
            }

        } else if(strcmp(arg, "-v") == 0 || strcmp(arg, "--verbose") == 0) {
            config->verbosity = Verbosity::VERBOSE;
            if(i+1 < argc) {
                char* ca_level = argv[i+1];
                i++;
                try{
                    int level = std::atoi(ca_level);

                    switch(level) {
                    case 1:
                        config->verbosity = Verbosity::NORMAL;
                        break;
                    case 2:
                        config->verbosity = Verbosity::VERBOSE;
                        break;
                    case 3:
                        config->verbosity = Verbosity::DEBUG;
                        break;
                    default:
                        std::cout << "Verbosity not in range from 1 to 3" << std::endl;
                    }
                } catch (...) {}
            }

        } else if(strcmp(arg, "-sm") == 0 || strcmp(arg, "--save-masks") == 0) {
            config->save_masks = true;
            config->masks_dir = "";
        } else if(strcmp(arg, "-mp") == 0 || strcmp(arg, "--masks-path") == 0) {
            if(i+1 < argc && std::filesystem::exists(argv[i+1])) {
                config->save_masks = true;
                config->masks_dir = argv[i+1];
                i++;
            } else {
                std::cout << "No mask directory provided after mask directory specifier." << std::endl;
                return false;
            }

        } else if(strcmp(arg, "-t") == 0 || strcmp(arg, "--threads") == 0) {
            if(i+1 < argc) {
                char* ca_threads = argv[i+1];
                i++;
                try{
                    int threads = std::atoi(ca_threads);
                    if(threads < 1) threads = 1;
                    if(threads > 32) threads = 32; 
                    config->threads = threads;
                } catch (...) {}
            }

        } else if(strcmp(arg, "-h") == 0 || strcmp(arg, "--help") == 0) {
            std::cout << help << std::endl;
            return false;

        } else {
            std::cout << "Parameter not recognized. Please use -h | --help for more information." << std::endl;
            return false;
        }
    }
    return true;
}