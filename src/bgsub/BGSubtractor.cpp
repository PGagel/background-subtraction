#include "bgsub/BGSubtractor.h"
#include <algorithm>
#include <sstream>
#include <iostream>
#include <thread>

namespace fs = std::filesystem;
typedef std::vector<fs::path> Paths;
typedef std::vector<cv::Mat> Images;

static std::vector<std::string> IMG_EXTENSIONS = {".bmp", ".pgm", ".pbm", ".ppm", ".sr", ".ras", ".jpeg", ".jpg", ".jpe", ".jp2", ".tiff", ".tif", ".png"};

BGSubtractor::BGSubtractor(fs::path bg_img_dir, fs::path fg_img_dir, fs::path out_dir, int threads, bool save_mask, fs::path mask_dir) : 
m_bg_img_dir(bg_img_dir), m_fg_img_dir(fg_img_dir), m_out_dir(out_dir), m_save_masks(save_mask), m_masks_dir(mask_dir), m_threads(threads) {}

int BGSubtractor::run() {
    Log::log("Finding Images...");
    // Attain all images located in the background image path
    Log::log_v("Looking for background images");
    Paths bg_image_paths = get_images_in_path(m_bg_img_dir);
    // Attain all images located in the normal image path
    Log::log_v("Looking for foreground images");
    Paths fg_image_paths = get_images_in_path(m_fg_img_dir);
    

    Log::log("Matching Images...");
    auto matched_paths = match_images(bg_image_paths, fg_image_paths);
    Log::log("Loading Images...");
    auto images = load_images(matched_paths);
    Log::log("Subtracting Images...");
    auto subtracted_images = subtract_images(images);
    //auto subtracted_images = subtract_images_naive(images);

    Log::log("Saving Images...");
    return save_images(subtracted_images);
}

Paths BGSubtractor::get_images_in_path(fs::path path) {
    Paths paths;
    for (const auto & entry : fs::directory_iterator(path)) {
        auto path = entry.path();
        auto extension = path.extension();


        Log::log_d("Looking at file: " + path.string());
        auto extension_it = std::find(IMG_EXTENSIONS.begin(), IMG_EXTENSIONS.end(), extension.string());
        if(extension_it != IMG_EXTENSIONS.end()) {
            Log::log_d("Accepted: '" + path.string() + "' as image.");
            paths.push_back(path);
        }
    }

    std::stringstream ss;
    ss << "Found " << paths.size() << " images in '" << path << "'.";
    Log::log_v(ss.str());

    return paths;
}

std::map<fs::path, fs::path> BGSubtractor::match_images(const Paths& bg_image_paths, const Paths& normal_image_paths) {
    std::map<fs::path, fs::path> matched_paths;

    for(const auto bg_path : bg_image_paths) {
        // filename without extension
        auto bg_name = bg_path.stem();

        Log::log_d("Looking for match for: '" + bg_path.string() + "'.");

        for(const auto fg_path : normal_image_paths) {
            // filename without extension
            auto fg_name = fg_path.stem();
            
            Log::log_d("\tComparing with: '" + fg_path.string() + "'.");
            // to lower would be a good idea here
            if(bg_name.string() == fg_name.string()) {
                Log::log_d("\tFound Match!");
                matched_paths[bg_path] = fg_path;
            }
        }
    }

    std::stringstream ss;
    ss << "Matched " << matched_paths.size() << " matches.";
    Log::log_v(ss.str());

    return matched_paths;
}

std::vector<std::pair<cv::Mat, cv::Mat>>  BGSubtractor::load_images(const std::map<fs::path, fs::path>& matched_image_paths) {
    std::vector<std::pair<cv::Mat, cv::Mat>> matched_images;

    int counter = 0;
    for(auto path_pair : matched_image_paths) {
        std::stringstream ss;
        ss << "Loading at image pair: '" << counter++ << "'.";
        Log::log_d(ss.str());

        Log::log_d("\tLoading image: '" + path_pair.first.string() + "'.");
        auto bg_img = cv::imread(path_pair.first.string());
        Log::log_d("\tLoading image: '" + path_pair.second.string() + "'.");
        auto normal_img = cv::imread(path_pair.second.string());
        matched_images.push_back(std::make_pair(bg_img, normal_img));
    }

    std::stringstream ss;
    ss << "Loaded " << matched_images.size() * 2 << " images.";
    Log::log_v(ss.str());

    return matched_images;
}

Images BGSubtractor::subtract_images_naive(const std::vector<std::pair<cv::Mat, cv::Mat>>& matched_images) {
    static int max_diff = 5;
    Images images;

    // this has high potential of being executed in parallel.
    // please do this in the future.
    // it hurts to write this code.
    int counter = 0;

    for(auto image_pair : matched_images) {
        std::stringstream ss;
        ss << "Looking at image pair: '" << counter++ << "'.";
        Log::log_d(ss.str());

        auto bg = image_pair.first;
        auto normal = image_pair.second;
        cv::Mat mask(bg.rows, bg.cols, CV_8UC1, cv::Scalar(0));        
        
        uchar* bg_data = bg.data;
        uchar* normal_data = normal.data;
        uchar* mask_data = mask.data;

        while(bg_data < bg.dataend) {

            // probably optimizable with SIMD
            int a = abs(bg_data[0] - normal_data[0]);
            int b = abs(bg_data[1] - normal_data[1]);
            int c = abs(bg_data[2] - normal_data[2]);
            *mask_data = (uchar) (255 * ((a>max_diff) && (b>max_diff) && (c>max_diff)));

            // increase pointer to next pixel
            mask_data ++;
            normal_data += 3;
            bg_data += 3;
        }

        if(m_save_masks) {
            m_masks.push_back(mask);
        }

        // actually subtract image
        cv::Mat result;
        cv::bitwise_and(normal, normal, result, mask);
        images.push_back(result);
    }

    std::stringstream ss;
    ss << "Subtracted " << images.size() << " images.";
    Log::log_v(ss.str());

    return images;
}

Images BGSubtractor::subtract_images(std::vector<std::pair<cv::Mat, cv::Mat>>& matched_images) {
    Images images;

    // this has high potential of being executed in parallel.
    // please do this in the future.
    // it hurts to write this code.
    int counter = 0;

    std::vector<std::thread> workers(m_threads);
    std::mutex in_queue_mutex;
    std::mutex out_queue_mutex;
    std::mutex mask_list_mutex;

    for(int i = 0; i < m_threads; i++) {

        std::stringstream ss;
        ss << "Creating Thread: " << counter++ << std::endl;
        Log::log_d(ss.str());

        workers[i] = std::thread([&matched_images, &images, &in_queue_mutex, &out_queue_mutex, &mask_list_mutex, this]() mutable {

            cv::Ptr<cv::BackgroundSubtractor> back_sub;
            back_sub = cv::createBackgroundSubtractorMOG2();
            
            while (true) {
                in_queue_mutex.lock();
                if (matched_images.size() == 0) {
                    in_queue_mutex.unlock();
                    break;
                }
                auto img_pair = matched_images.back();
                matched_images.pop_back();
                in_queue_mutex.unlock();

                // train subtractor
                for(int i = 0; i < 16; i++) {
                    cv::Mat out;
                    back_sub->apply(img_pair.first, out, 0.5f);
                }

                // get mask from bg check
                cv::Mat mask;
                back_sub->apply(img_pair.second, mask, 0);
                cv::threshold(mask, mask, 127.0, 255.0, cv::THRESH_BINARY);

                // save mask if specified
                if(this->m_save_masks) {
                    mask_list_mutex.lock();
                    this->m_masks.push_back(mask);
                    mask_list_mutex.unlock();
                }

                // actually subtract image
                cv::Mat result;
                cv::bitwise_and(img_pair.second, img_pair.second, result, mask);

                out_queue_mutex.lock();
                images.push_back(result);
                out_queue_mutex.unlock();
            }
        });
    }

    counter = 0;
    for (auto& worker : workers) {
        std::stringstream ss;
        ss << "Joining Thread: " << counter++ << std::endl;
        Log::log_d(ss.str());

        worker.join();
    }
    
    std::stringstream ss;
    ss << "Subtracted " << images.size() << " images.";
    Log::log_v(ss.str());

    return images;
}

int BGSubtractor::save_images(const std::vector<cv::Mat>& images) {
    int counter = 0;

    for(auto image : images) {
        std::stringstream ss;
        ss << "image_" << std::setw(3) << std::setfill('0') << counter++ << ".png";
        Log::log_d(ss.str());

        cv::imwrite((m_out_dir / ss.str()).string(), image);

    }
    std::stringstream ss;
    ss << "Saved " << images.size() << " subtracted images.";
    Log::log_v(ss.str());

    if(m_save_masks) {
        for(auto mask : m_masks) {
            std::stringstream ss;
            ss << "mask_" << std::setw(3) << std::setfill('0') << counter++ << ".png";
            Log::log_d(ss.str());

            cv::imwrite((m_masks_dir / ss.str()).string(), mask);
        }
            
        std::stringstream ss;
        ss << "Saved " << m_masks.size() << " masks.";
        Log::log_v(ss.str());
    }

    return counter;
}