#include "bgsub/Log.h"

Verbosity Log::verbosity_level = Verbosity::NORMAL;

void Log::log(std::string message) {
    _log(Verbosity::NORMAL, message);
}

void Log::log_v(std::string message) {
    _log(Verbosity::VERBOSE, message);
}

void Log::log_d(std::string message) {
    _log(Verbosity::DEBUG, message);
}

void Log::_log (Verbosity level, std::string message) {
    static time_t timer;
    static char time_buffer[26];
    static struct tm* tm_info;

    if(level > Log::verbosity_level) return;

    timer = time(NULL);
    tm_info = localtime(&timer);

    strftime(time_buffer, 26, "%H:%M:%S", tm_info);

    printf("%s [%s] %s %s\n", time_buffer, Log::verbosity_strings[(int)level], prompt, message.c_str());
}